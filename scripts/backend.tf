# Sets the "backend" used to store Terraform state.
# This is required to make continous delivery work.

terraform {
    backend "azurerm" {
        resource_group_name  = "flixtubeStratistier"
        storage_account_name = "flixtubestratistier"
        container_name       = "terraform-tfstate"
        key                  = "terraform.tfstate"
    }
} 